package br.com.skipthedishes.infrastructure.redis;

import br.com.skipthedishes.domain.Order;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.net.HostAndPort;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.connection.RedisSentinelConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;

import java.net.URISyntaxException;
import java.util.List;

@Profile("redis")
@Configuration
@EnableConfigurationProperties
public class RedisConfig {

    @Autowired
    private RedisClusterProperties clusterProperties;

    @Bean
    public JedisConnectionFactory connectionFactory() throws URISyntaxException {
        if (this.clusterProperties.isEnabled()) {

            RedisSentinelConfiguration sentinelConfig = new RedisSentinelConfiguration()
                    .master("skipthedishesmaster");
            this.clusterProperties.getNodes().forEach(node -> {
                HostAndPort hostAndPort = HostAndPort.fromString(node);
                sentinelConfig.sentinel(hostAndPort.getHost(), hostAndPort.getPort());
            });

            JedisConnectionFactory factory = new JedisConnectionFactory(sentinelConfig);

            return factory;

        } else {
            JedisConnectionFactory connectionFactory = new JedisConnectionFactory();
            HostAndPort hostAndPort = HostAndPort.fromString(this.clusterProperties.getNodes().get(0));
            connectionFactory.setHostName(hostAndPort.getHost());
            connectionFactory.setPort(hostAndPort.getPort());
            return connectionFactory;
        }
    }

    @Bean
    @Primary
    public <T> RedisTemplate<String, T> redisTemplate(ObjectMapper mapper) throws URISyntaxException {
        RedisTemplate<String, T> redisTemplate = new RedisTemplate<>();

        ObjectMapper redisMapper = mapper.copy();

        redisTemplate.setConnectionFactory(connectionFactory());
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new JsonRedisSerializer(redisMapper));
        redisTemplate.setEnableDefaultSerializer(false);
        redisTemplate.afterPropertiesSet();

        return redisTemplate;
    }

    static class JsonRedisSerializer implements RedisSerializer<Order> {

        private final ObjectMapper mapper;

        public JsonRedisSerializer(ObjectMapper mapper) {
            this.mapper = mapper;
        }

        @Override
        public byte[] serialize(Order t) throws SerializationException {
            try {
                return this.mapper.writeValueAsBytes(t);
            } catch (JsonProcessingException e) {
                throw new SerializationException(e.getMessage(), e);
            }
        }

        @Override
        public Order deserialize(byte[] bytes) throws SerializationException {

            if (bytes == null) {
                return null;
            }

            try {
                return this.mapper.readValue(bytes, Order.class);
            } catch (Exception e) {
                throw new SerializationException(e.getMessage(), e);
            }
        }
    }

    @Setter
    @Getter
    @Component
    @ConfigurationProperties(prefix = "spring.redis.cluster")
    public static class RedisClusterProperties {

        private List<String> nodes;
        private String database;
        private boolean enabled;
    }
}
