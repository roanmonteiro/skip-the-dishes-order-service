package br.com.skipthedishes.infrastructure.redis;

import br.com.skipthedishes.domain.Order;
import br.com.skipthedishes.domain.repository.OrderRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Slf4j
@Repository
@Profile("redis")
@AllArgsConstructor(onConstructor = @__({@Autowired}))
public class RedisOrderRepository implements OrderRepository {

    @Autowired
    private RedisTemplate<String, Order> template;


    @Override
    public void set(Order order) {
        this.template.opsForValue().set(order.getId(), order, 60, TimeUnit.MINUTES);
        log.info("Adding key {} , value={}", order.getId(), order.toString());
    }

    @Override
    public Optional<Order> get(String orderId) {
        Order order = this.template.opsForValue().get(orderId);

        if (order == null) {
            return Optional.empty();
        }

        log.info("Getting key {} , value={}", orderId, order.toString());
        return Optional.of(order);
    }

    @Override
    public void del(String orderId) {
        this.template.delete(orderId);
        log.info("Deleting key {} ", orderId);
    }
}
