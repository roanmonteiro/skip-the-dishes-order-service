package br.com.skipthedishes.infrastructure;


import br.com.skipthedishes.api.OrderAPI;
import br.com.skipthedishes.domain.Order;
import br.com.skipthedishes.domain.repository.OrderRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Provider;
import javax.servlet.http.HttpServletRequest;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@RestController
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class OrderControllerV1 implements OrderAPI {

    private Provider<HttpServletRequest> requestProvider;

    @Autowired
    private OrderRepository repository;

    @Override
    @PostMapping(value = "/api/v1/Order", consumes = "application/json")
    public ResponseEntity<String> receiveOrders(@RequestBody Order order) {
        long startTime = System.currentTimeMillis();

        order.setId(UUID.randomUUID().toString());
        this.repository.set(order);

        log.info("Receiving a new order [userAgent=\"{}\", Order={}, elapsedTime={}]",
                userAgent(), order.toString(), this.elapsedTimeSince(startTime));
        return new ResponseEntity<>(order.getId(), HttpStatus.CREATED);
    }

    @Override
    @DeleteMapping(value = "/api/v1/Order/{orderId}")
    public ResponseEntity cancelOrders(@PathVariable("orderId") String orderId) {
        long startTime = System.currentTimeMillis();

        this.repository.del(orderId);

        log.info("Canceling an order [userAgent=\"{}\", Order Id={}, elapsedTime={}]",
                userAgent(), orderId, this.elapsedTimeSince(startTime));
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @Override
    @GetMapping(value = "/api/v1/Order/{orderId}")
    public ResponseEntity<Order> getOrderStatus(@PathVariable("orderId") String orderId) {

        long startTime = System.currentTimeMillis();

        Optional<Order> orderOptional = this.repository.get(orderId);

        log.info("Getting order status [userAgent=\"{}\", Order Id={}, elapsedTime={}]",
                userAgent(), orderId, this.elapsedTimeSince(startTime));

        return orderOptional
                .map(order -> new ResponseEntity<>(order, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    private String elapsedTimeSince(long startTime) {
        return String.valueOf(System.currentTimeMillis() - startTime);
    }

    private String userAgent() {
        return Optional.ofNullable(this.requestProvider.get())
                .map(request -> request.getHeader("X-User-Agent"))
                .orElse("not specified");
    }
}
