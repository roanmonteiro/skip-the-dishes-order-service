package br.com.skipthedishes.infrastructure.config;

import com.google.common.base.Joiner;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Configuration
public class CrossOriginConfig {

    @Bean
    public WebMvcConfigurer corsConfigurer(Properties properties) {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                List<String> origins = properties.allowedOrigins;
                registry.addMapping("/**")
                        .allowedOrigins(origins.toArray(new String[origins.size()]))
                        .allowedMethods("GET", "POST", "DELETE");
                log.info("CORS: Allowing origins {}", Joiner.on(" | ").join(properties.allowedOrigins));
            }
        };
    }

    @Getter
    @Setter
    @Component
    @ConfigurationProperties(prefix = "http.cors")
    public static class Properties {
        private List<String> allowedOrigins = new ArrayList<>();
    }
}