package br.com.skipthedishes.api.exception;

import static br.com.skipthedishes.api.exception.Exceptions.generateMessage;
import static br.com.skipthedishes.api.exception.Exceptions.toMap;

public class EntityNotFoundException extends Exception {

    public EntityNotFoundException(Class clazz, String... searchParamsMap) {
        super(generateMessage(clazz.getSimpleName(), toMap(String.class, String.class, searchParamsMap)));
    }
}
