package br.com.skipthedishes.api.exception;

import static br.com.skipthedishes.api.exception.Exceptions.toMap;

public class ServerException extends Exception {

    public ServerException(String... searchParamsMap) {
        super("Internal server error " + toMap(String.class, String.class, searchParamsMap));
    }
}
