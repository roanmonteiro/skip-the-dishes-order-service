package br.com.skipthedishes.api;

import br.com.skipthedishes.domain.Order;
import org.springframework.http.ResponseEntity;

public interface OrderAPI {

    ResponseEntity<String> receiveOrders(Order order);

    ResponseEntity cancelOrders(String id);

    ResponseEntity<Order> getOrderStatus(String id);
}
