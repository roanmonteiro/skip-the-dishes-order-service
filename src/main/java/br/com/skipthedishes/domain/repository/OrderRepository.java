package br.com.skipthedishes.domain.repository;

import br.com.skipthedishes.domain.Order;

import java.util.Optional;

public interface OrderRepository {

    void set(Order order);

    Optional<Order> get(String orderId);

    void del(String orderId);
}
