package br.com.skipthedishes.domain;

import lombok.*;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class OrderItem {

    @Id
    private long id;

    @NotNull
    private String orderId;

    @NotNull
    private long productId;

    private Product product;

    @NotNull
    private double price;

    @NotNull
    private long quantity;

    private double total;
}
