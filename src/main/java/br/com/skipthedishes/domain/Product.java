package br.com.skipthedishes.domain;

import lombok.*;
import org.springframework.data.annotation.Id;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class Product {

    @Id
    private long id;

    private long storeId;

    private String name;

    private String description;

    private double price;
}
