package br.com.skipthedishes.infrastructure;

import br.com.skipthedishes.domain.Order;
import br.com.skipthedishes.domain.OrderItem;
import br.com.skipthedishes.domain.Product;
import br.com.skipthedishes.domain.repository.OrderRepository;
import br.com.skipthedishes.infrastructure.redis.RedisConfig;
import br.com.skipthedishes.infrastructure.redis.RedisOrderRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import redis.embedded.RedisServer;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(properties = {
        "spring.redis.cluster.nodes[0]=localhost:6379",
        "spring.redis.cluster.enabled=false"
})
@RunWith(SpringRunner.class)
@ActiveProfiles("redis")
public class RedisOrderRepositoryTest {

    private static RedisServer server;

    @Autowired
    private OrderRepository repository;

    @Autowired
    private RedisTemplate template;

    @BeforeClass
    public static void setUp() throws IOException {
        server = RedisServer.builder().port(6738).build();
        System.out.println(server.toString());
        System.out.println(server.isActive());
        server.start();
    }

    @Test
    public void putAndRecover() {
        Order order = new Order();
        OrderItem orderItem = new OrderItem();
        Product product = new Product();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

        order.setId(UUID.randomUUID().toString());
        order.setDate(LocalDateTime.parse("2018-03-18T19:14:54.890Z", formatter));
        order.setCustomerId(1);
        order.setDeliveryAddress("Street Unit 123");
        order.setContact("Andrew");
        order.setStoreId(1);
        order.setStatus("Preparing");
        order.setLastUpdate(LocalDateTime.parse("2018-03-18T19:14:54.890Z", formatter));
        order.setTotal(1000.00);

        orderItem.setId(1);
        orderItem.setOrderId(order.getId());
        orderItem.setProductId(1);
        orderItem.setPrice(100.00);
        orderItem.setQuantity(10);
        orderItem.setTotal(1000.00);

        product.setId(1);
        product.setName("Sushi");
        product.setPrice(100.00);
        product.setStoreId(1);

        orderItem.setProduct(product);
        List<OrderItem> orderItems = new ArrayList<>();
        orderItems.add(orderItem);

        order.setOrderItems(orderItems);

        this.repository.set(order);

        Optional<Order> orderOptional = this.repository.get(order.getId());
        String uuid = orderOptional
                .map(o -> o.getId())
                .orElseGet(null);

        assertThat(uuid).isEqualTo(order.getId());

    }

    @Test
    public void putAndDelete() {
        Order order = new Order();
        OrderItem orderItem = new OrderItem();
        Product product = new Product();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

        order.setId(UUID.randomUUID().toString());
        order.setDate(LocalDateTime.parse("2018-03-18T19:14:54.890Z", formatter));
        order.setCustomerId(1);
        order.setDeliveryAddress("Street Unit 123");
        order.setContact("Andrew");
        order.setStoreId(1);
        order.setStatus("Preparing");
        order.setLastUpdate(LocalDateTime.parse("2018-03-18T19:14:54.890Z", formatter));
        order.setTotal(1000.00);

        orderItem.setId(1);
        orderItem.setOrderId(order.getId());
        orderItem.setProductId(1);
        orderItem.setPrice(100.00);
        orderItem.setQuantity(10);
        orderItem.setTotal(1000.00);

        product.setId(1);
        product.setName("Sushi");
        product.setPrice(100.00);
        product.setStoreId(1);

        orderItem.setProduct(product);
        List<OrderItem> orderItems = new ArrayList<>();
        orderItems.add(orderItem);

        order.setOrderItems(orderItems);

        this.repository.set(order);

        this.repository.del(order.getId());

        Optional<Order> orderOptional = this.repository.get(order.getId());
        String uuid = orderOptional
                .map(o -> o.getId())
                .orElse(null);
        assertThat(uuid).isEqualTo(null);
    }

    @AfterClass
    public static void shutdown() {
        server.stop();
    }

    @Configuration
    @Import({RedisOrderRepository.class, RedisConfig.class})
    static class Config {
        @Bean
        public ObjectMapper mapper() {
            return new ObjectMapper();
        }
    }
}
