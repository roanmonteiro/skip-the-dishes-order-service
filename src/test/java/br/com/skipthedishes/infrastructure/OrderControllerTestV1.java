package br.com.skipthedishes.infrastructure;

import br.com.skipthedishes.domain.Order;
import br.com.skipthedishes.domain.OrderItem;
import br.com.skipthedishes.domain.Product;
import br.com.skipthedishes.domain.repository.OrderRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(OrderControllerV1.class)
public class OrderControllerTestV1 {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private OrderRepository repository;

    @Test
    @WithMockUser(roles = "USER", username = "skipthedishes", password = "skipthedishes")
    public void getOrder() throws Exception {
        Order order = new Order();
        OrderItem orderItem = new OrderItem();
        Product product = new Product();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

        order.setId(UUID.randomUUID().toString());
        order.setDate(LocalDateTime.parse("2018-03-18T19:14:54.890Z", formatter));
        order.setCustomerId(1);
        order.setDeliveryAddress("Street Unit 123");
        order.setContact("Andrew");
        order.setStoreId(1);
        order.setStatus("Preparing");
        order.setLastUpdate(LocalDateTime.parse("2018-03-18T19:14:54.890Z", formatter));
        order.setTotal(1000.00);

        orderItem.setId(1);
        orderItem.setOrderId(order.getId());
        orderItem.setProductId(1);
        orderItem.setPrice(100.00);
        orderItem.setQuantity(10);
        orderItem.setTotal(1000.00);

        product.setId(1);
        product.setName("Sushi");
        product.setPrice(100.00);
        product.setStoreId(1);

        orderItem.setProduct(product);
        List<OrderItem> orderItems = new ArrayList<>();
        orderItems.add(orderItem);

        order.setOrderItems(orderItems);

        when(this.repository.get(any())).thenReturn(Optional.of(order));

        mvc.perform(get("/api/v1/Order/" + order.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(result -> {
                    String json = result.getResponse().getContentAsString();
                    Order value = new ObjectMapper().readValue(json, new TypeReference<Order>() {
                    });
                    assertThat(value).isEqualTo(order);
                });
    }

    @Test
    public void withoutAuthentication() throws Exception {
        Order order = new Order();
        OrderItem orderItem = new OrderItem();
        Product product = new Product();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

        order.setId(UUID.randomUUID().toString());
        order.setDate(LocalDateTime.parse("2018-03-18T19:14:54.890Z", formatter));
        order.setCustomerId(1);
        order.setDeliveryAddress("Street Unit 123");
        order.setContact("Andrew");
        order.setStoreId(1);
        order.setStatus("Preparing");
        order.setLastUpdate(LocalDateTime.parse("2018-03-18T19:14:54.890Z", formatter));
        order.setTotal(1000.00);

        orderItem.setId(1);
        orderItem.setOrderId(order.getId());
        orderItem.setProductId(1);
        orderItem.setPrice(100.00);
        orderItem.setQuantity(10);
        orderItem.setTotal(1000.00);

        product.setId(1);
        product.setName("Sushi");
        product.setPrice(100.00);
        product.setStoreId(1);

        orderItem.setProduct(product);
        List<OrderItem> orderItems = new ArrayList<>();
        orderItems.add(orderItem);

        order.setOrderItems(orderItems);

        when(this.repository.get(any())).thenReturn(Optional.of(order));

        mvc.perform(get("/api/v1/Order/" + order.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }


    @Before
    public void setupMock() {
        MockitoAnnotations.initMocks(this);
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
